<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

namespace Tygh\Shippings\Services;

use Tygh\Registry;
use Tygh\Shippings\IService;
use Tygh\Http;

class CalcByDistance implements IService
{
    /**
     * Availability multithreading in this module
     *
     * @var bool $_allow_multithreading
     */
    private $_allow_multithreading = false;

    /**
     * The currency in which the carrier calculates shipping costs.
     *
     * @var string $calculation_currency
     */
    public $calculation_currency = 'RUB';

    /**
     * Stack for errors occured during the preparing rates process
     *
     * @var array $_error_stack
     */
    private $_error_stack = array();

    private function _internalError($error)
    {
        $this->_error_stack[] = $error;
    }

    /**
     * Sets data to internal class variable
     *
     * @param array $shipping_info
     * @return array|void
     */
    public function prepareData($shipping_info)
    {
        $this->_shipping_info = $shipping_info;
    }

    /**
     * Gets shipping cost and information about possible errors
     *
     * @param string $response
     * @return array Shipping cost and errors
     * @internal param string $resonse Reponse from Shipping service server
     */
    public function processResponse($response)
    {
        $return = array(
            'cost' => false,
            'error' => false,
            'delivery_time' => false,
        );

        $response = json_decode($response, true);

        if (!empty($response) && empty($response["errors"])) {
            $rows = array_shift($response["rows"]);
            $element = array_shift($rows["elements"]);
        } else {
            return $return;
        }

        if ($element["status"] !== "FAIL") {

            $amount = $element["distance"]["value"];

            $shipping = $this->_shipping_info;

            $rate_info = db_get_row(
                'SELECT rate_id, rate_value FROM ?:shipping_rates'
                . ' WHERE shipping_id = ?i'
                . ' ORDER BY destination_id desc',
                $shipping['shipping_id']
            );

            $result_cost = 0;

            if (!empty($rate_info)) {
                $rate_info['rate_value'] = unserialize($rate_info['rate_value']);

                $rate_value = array_reverse($rate_info['rate_value'][DISTANCE_RATE_INDEX], true);
                $base_cost = $shipping['package_info']['C'];

                foreach ($rate_value as $rate_amount => $data) {

                    if ($rate_amount < $amount || ($rate_amount == 0.00 && $amount == 0.00)) {
                        $value = $data['type'] == 'F' ? $data['value'] : (($base_cost * $data['value']) / 100);
                        $per_unit = (!empty($data['per_unit']) && $data['per_unit'] == 'Y') ? $shipping['package_info'][DISTANCE_RATE_INDEX] : 1;

                        $result_cost += $value * $per_unit;
                        break;
                    }
                }
            }

            $return['cost'] = $result_cost;
            //$return['delivery_time'] = ceil($element["duration"]["value"] / SECONDS_IN_DAY);
        } else {
            $return['error'] = $this->processErrors($element);
        }

        return $return;
    }

    /**
     * Gets error message from shipping service server
     *
     * @param string $response
     * @return string Text of error or false if no errors
     * @internal param string $resonse Reponse from Shipping service server
     */
    public function processErrors($response)
    {
        $error = __('error_occurred');
        
        if ($response["status"] === "FAIL") {
            $error = __('route_not_found');
        }

        return $error;
    }

    /**
     * Checks if shipping service allows to use multithreading
     *
     * @return bool true if allow
     */
    public function allowMultithreading()
    {
        return $this->_allow_multithreading;
    }

    /**
     * Prepare request information
     *
     * @return array Prepared data
     */
    public function getRequestData()
    {
        $origination = $this->_shipping_info['package_info']['origination'];
        $location = $this->_shipping_info['package_info']['location'];

        if (empty($origination["country"]) || empty($location["country"])) {
            return false;
        }

        $location_address = fn_get_country_name($location["country"]);
        $location_address .= !empty($location["state"]) ? ", " . fn_get_state_name($location["state"], $location["country"]) : '';
        $location_address .= !empty($location["city"]) ? ", " . $location["city"] : '';
        $location_address .= !empty($location["address"]) ? ", " . $location["address"] : '';

        $origination_address = fn_get_country_name($origination["country"]);
        $origination_address .= !empty($origination["state"]) ? ", " . fn_get_state_name($origination["state"], $origination["country"]) : '';
        $origination_address .= !empty($origination["city"]) ? ", " . $origination["city"] : '';
        $origination_address .= !empty($origination["address"]) ? ", " . $origination["address"] : '';

        $origination_coords = $this->getGeoByAddress($origination_address);
        $location_coords = $this->getGeoByAddress($location_address);

        if (empty($origination_coords) || empty($location_coords)) {
            $this->_internalError(__('error_connection_geocoder'));
        }

        $url = YANDEX_API_DISTANCE_MATRIX_ADDR;
        $request = array(
            'apikey' => !empty($this->_shipping_info['service_params']['api_key']) ? $this->_shipping_info['service_params']['api_key'] : '',
            'origins' => is_array($origination_coords) ? array_pop($origination_coords) . "," . array_pop($origination_coords) : array(),
            'destinations' => is_array($location_coords) ? array_pop($location_coords) . "," . array_pop($location_coords) : array(),
            'mode' => Registry::get("addons.delivery_cost_by_distance.delivery_transport")
        );

        $request_data = array(
            'method' => 'post',
            'url' => $url,
            'data' => $request,
        );

        return $request_data;
    }

    /**
     * Process simple request to shipping service server
     *
     * @return string Server response
     */
    public function getSimpleRates()
    {
        $extra = array(
            'log_preprocessor' => '\Tygh\Http::unescapeJsonResponse'
        );

        $data = $this->getRequestData();

        // Russian post server works very unstably, that is why we cannot use multithreading.
        $key = md5(serialize($data['data']));
        $response = fn_get_session_data($key);

        if (empty($response) && !empty($data)) {
            $response = Http::get($data['url'], $data['data'], $extra);
            fn_set_session_data($key, $response);
        }

        return $response;
    }

    public function prepareAddress($address)
    {
        
    }

    public static function getInfo()
    {
        return array(
            'name' => __('carrier_calc_by_distance'),
            'tracking_url' => ''
        );
    }

    /**
     * Request the geographic coordinates of an address from Yandex
     *
     * @param  array $address The array with the address information
     * @return array The most probable geographic coordinates of the address
     */
    public function getGeoByAddress($address)
    {
        $url = "https://geocode-maps.yandex.ru/1.x/";
        $data = array(
            'geocode' => $address,
            'format' => 'json',
            'results' => 2,
            'sco' => 'longlat',
            'apikey' => !empty($this->_shipping_info['service_params']['geocoder_api_key']) ? $this->_shipping_info['service_params']['geocoder_api_key'] : '',
        );

        $response = Http::get($url, $data, array(
            'log_preprocessor' => '\Tygh\Http::unescapeJsonResponse'
        ));

        $response = json_decode($response, true);
        $response = isset($response['response']['GeoObjectCollection']) ? $response['response']['GeoObjectCollection'] : null;

        $ll_address = false;

        if ($response && $response['metaDataProperty']['GeocoderResponseMetaData']['found'] > 0) {
            $object = reset($response['featureMember']);
            $object = $object['GeoObject'];

            $ll_address = explode(' ', $object['Point']['pos']);
        }

        return $ll_address;
    }

    /**
     * Fetches ISO 3166-1 (numeric-3) country code
     *
     * @param  string $country
     * @return string
     */
    private function getCountryCode($country)
    {
        $country_code = '';

        if (!empty($country)) {
            $country_code = db_get_field('SELECT code_N3 FROM ?:countries WHERE code = ?s', $country);
        }

        return $country_code;
    }
}
