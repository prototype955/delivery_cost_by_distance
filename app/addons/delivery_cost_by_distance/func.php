<?php
/***************************************************************************
 *                                                                          *
 *   (c) 2004 Vladimir V. Kalynyak, Alexey V. Vinokurov, Ilya M. Shalnev    *
 *                                                                          *
 * This  is  commercial  software,  only  users  who have purchased a valid *
 * license  and  accept  to the terms of the  License Agreement can install *
 * and use this program.                                                    *
 *                                                                          *
 ****************************************************************************
 * PLEASE READ THE FULL TEXT  OF THE SOFTWARE  LICENSE   AGREEMENT  IN  THE *
 * "copyright.txt" FILE PROVIDED WITH THIS DISTRIBUTION PACKAGE.            *
 ****************************************************************************/

use Tygh\Languages\Languages;

if (!defined('BOOTSTRAP')) { die('Access denied'); }

function fn_delivery_cost_by_distance_install()
{
    $service = array(
        'status' => 'A',
        'module' => CALC_BY_DISTANCE_MODULE_NAME,
        'code' => 'yandex_api',
        'sp_file' => '',
        'description' => 'Yandex',
    );

    $service['service_id'] = db_get_field('SELECT service_id FROM ?:shipping_services WHERE module = ?s AND code = ?s', $service['module'], $service['code']);

    if (empty($service['service_id'])) {
        $service['service_id'] = db_query('INSERT INTO ?:shipping_services ?e', $service);
    }

    $languages = Languages::getAll();
    
    foreach ($languages as $lang_code => $lang_data) {

        if ($lang_code == 'ru') {
            $service['description'] = "Яндекс";
        } else {
            $service['description'] = "Yandex";
        }

        $service['lang_code'] = $lang_code;

        db_query('INSERT INTO ?:shipping_service_descriptions ?e', $service);
    }
}

function fn_delivery_cost_by_distance_uninstall()
{
    $service_ids = db_get_fields('SELECT service_id FROM ?:shipping_services WHERE module = ?s', CALC_BY_DISTANCE_MODULE_NAME);

    if (!empty($service_ids)) {
        db_query('DELETE FROM ?:shipping_services WHERE service_id IN (?a)', $service_ids);
        db_query('DELETE FROM ?:shipping_service_descriptions WHERE service_id IN (?a)', $service_ids);
    }
}

/**
 * Hook handler: save new rate type (by distance)
 */
function fn_delivery_cost_by_distance_update_shipping_post($shipping_data, $shipping_id, $lang_code, $action)
{
    if (!empty($shipping_id) && !empty($shipping_data['rates']) && fn_delivery_cost_by_distance_is_module_service($shipping_data['service_id'])) {

        foreach ($shipping_data['rates'] as $destination_id => $rate) {

            if (!empty($rate['destination_id'])) {
                $destination_id = $rate['destination_id'];
            }

            $rate_value = db_get_field("SELECT rate_value FROM ?:shipping_rates WHERE shipping_id = ?i AND destination_id = ?i", $shipping_id, $destination_id);

            $normalized_data = !empty($rate_value) ? unserialize($rate_value) : array();

            $type = DISTANCE_RATE_INDEX;

            // Update rate values
            if (!empty($rate['rate_value'][$type]) && is_array($rate['rate_value'][$type])) {
                fn_normalized_shipping_rate($normalized_data, $rate['rate_value'][$type], $type);
            }

            // Add new rate values
            if (!empty($shipping_data['add_rates']) && isset($shipping_data['add_rates'][$destination_id]['rate_value'][$type]) && is_array($shipping_data['add_rates'][$destination_id]['rate_value'][$type])) {
                fn_normalized_shipping_rate($normalized_data, $shipping_data['add_rates'][$destination_id]['rate_value'][$type], $type);
            }

            if (!empty($normalized_data[$type]) && is_array($normalized_data[$type])) {
                ksort($normalized_data[$type], SORT_NUMERIC);
            }

            if (is_array($normalized_data)) {

                foreach ($normalized_data as $k => $v) {

                    if ((count($v) == 1) && (floatval($v[0]['value']) == 0)) {
                        unset($normalized_data[$k]);
                        continue;
                    }
                }
            }

            if (!fn_is_empty($normalized_data)) {
                $normalized_data = serialize($normalized_data);
                db_query("REPLACE INTO ?:shipping_rates (rate_value, shipping_id, destination_id) VALUES(?s, ?i, ?i)", $normalized_data, $shipping_id, $destination_id);
            }
        }
    }
}

/**
 * Hook handler: get distance rate
 */
function fn_delivery_cost_by_distance_get_shipping_info_post($shipping_id, $lang_code, &$shipping)
{
    $rates = $shipping['rates'];

    if (!empty($rates)) {

        foreach ($rates as $key => $rate) {

            if (empty($rate['rate_value'][DISTANCE_RATE_INDEX][0])) {
                $shipping['rates'][$key]['rate_value'][DISTANCE_RATE_INDEX][0] = array();
            }
        }
    }
}

/**
 * Checker. If service owner = module, return true, else false
 *
 * @param int    $service_id Service id
 * @return bool  result of check
 */
function fn_delivery_cost_by_distance_is_module_service($service_id)
{
    $service = db_get_row('SELECT * FROM ?:shipping_services WHERE service_id = ?i', (int) $service_id);

    if ($service["module"] === CALC_BY_DISTANCE_MODULE_NAME) {
        return true;
    } else {
        return false;
    }
}
