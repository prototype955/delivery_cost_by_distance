<fieldset>
    <div class="control-group">
        <label class="control-label" for="api_key">{__("yandex_api_key")} 
            <a class="cm-tooltip" title="{__("yandex_api_key_tooltip")}"><i class="icon-question-sign"></i></a>
        </label>
        <div class="controls">
            <input id="api_key" type="text" name="shipping_data[service_params][api_key]" size="30" value="{$shipping.service_params.api_key}"/>
        </div>
    </div>
    <div class="control-group">
        <label class="control-label" for="geocoder_api_key">{__("geocoder_api_key")} 
            <a class="cm-tooltip" title="{__("geocoder_api_key_tooltip")}"><i class="icon-question-sign"></i></a>
        </label>
        <div class="controls">
            <input id="geocoder_api_key" type="text" name="shipping_data[service_params][geocoder_api_key]" size="30" value="{$shipping.service_params.geocoder_api_key}"/>
        </div>
    </div>
</fieldset>
