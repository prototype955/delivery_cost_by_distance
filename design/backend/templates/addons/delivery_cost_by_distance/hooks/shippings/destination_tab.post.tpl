{if $services[$shipping.service_id].module == $smarty.const.CALC_BY_DISTANCE_MODULE_NAME && $services[$shipping.service_id].code == 'yandex_api'}

    {include file="common/subheader.tpl" title=__("distance_dependences") meta="clear"}
    <div class="table-wrapper"> 
        <table class="table table-middle table--relative">
            <thead>
            <tr class="cm-first-sibling">
                <th width="1%">{include file="common/check_items.tpl" check_target="distance-`$destination_id`"}</th>
                <th width="30%">{__("order_distance")}</th>
                <th width="15%">{__("rate_value")}</th>
                <th width="15%">{__("type")}</th>
                <th>{hook name="shippings:distance_dependences_head"}{/hook}</th>
                <th>&nbsp;</th>
            </tr>
            </thead>

            {foreach from=$rate_data.rate_value[$smarty.const.DISTANCE_RATE_INDEX] item="rate" key="k" name="rdf"}
                <tr>
                    <td>
                        <input type="checkbox" name="delete_rate_data[{$destination_id}][{$smarty.const.DISTANCE_RATE_INDEX}][{$k}]" value="Y" {if $smarty.foreach.rdf.first}disabled="disabled"{/if} class=" cm-item-distance-{$destination_id} cm-item" /></td>
                    <td class="nowrap">
                        {__("more_than")}&nbsp;
                        {if $smarty.foreach.rdf.first}
                            <input type="hidden" name="shipping_data[rates][{$destination_id}][rate_value][{$smarty.const.DISTANCE_RATE_INDEX}][0][amount]" value="0" />
                            &nbsp;0 {__("meters")}
                        {else}
                            <input type="text" name="shipping_data[rates][{$destination_id}][rate_value][{$smarty.const.DISTANCE_RATE_INDEX}][{$k}][amount]" data-v-min="0" data-v-max="999999" data-p-sign="s" data-a-sign=" {__("meters")}" size="5" value="{$k}" class="cm-numeric input-small input-hidden" />
                        {/if}
                    </td>
                    <td>
                        <input type="text" name="shipping_data[rates][{$destination_id}][rate_value][{$smarty.const.DISTANCE_RATE_INDEX}][{if $smarty.foreach.rdf.first}0{else}{$k}{/if}][value]" size="5" value="{$rate.value|default:"0"}" class="input-small input-hidden" /></td>
                    <td>
                        <select class="input-medium" name="shipping_data[rates][{$destination_id}][rate_value][{$smarty.const.DISTANCE_RATE_INDEX}][{if $smarty.foreach.rdf.first}0{else}{$k}{/if}][type]">
                            <option value="F" {if $rate.type == "F"}selected="selected"{/if}>{__("absolute")} ({$currencies.$primary_currency.symbol nofilter})</option>
                            <option value="P" {if $rate.type == "P"}selected="selected"{/if}>{__("percent")} (%)</option>
                        </select></td>
                    <td>{hook name="shippings:distance_dependences_body"}{/hook}</td>
                    <td class="nowrap right">
                        <div class="hidden-tools">
                            {if $smarty.foreach.rdf.first}
                                {include file="buttons/remove_item.tpl" but_class="cm-delete-row"}
                            {else}
                                {include file="buttons/remove_item.tpl" only_delete='Y' but_class="cm-delete-row"}
                            {/if}

                        </div>
                    </td>
                </tr>
                {foreachelse}
                <tr class="no-items">
                    <td colspan="6">
                        <input type="hidden" name="shipping_data[rates][{$destination_id}][rate_value][{$smarty.const.DISTANCE_RATE_INDEX}][0][amount]" value="0" />
                        <input type="hidden" name="shipping_data[rates][{$destination_id}][rate_value][{$smarty.const.DISTANCE_RATE_INDEX}][0][value]" value="0" />
                        <input type="hidden" name="shipping_data[rates][{$destination_id}][rate_value][{$smarty.const.DISTANCE_RATE_INDEX}][0][type]" value="F" />
                        <p>{__("no_items")}</p></td>
                </tr>
            {/foreach}

            {if !$hide_for_vendor}
                <tr id="box_add_rate_celm_distance_{$destination_id}">
                    <td>
                        <input type="checkbox" disabled="disabled" value="Y" class=" cm-item-distance cm-item" /></td>
                    <td>
                        {__("more_than")}&nbsp;<input type="text" name="shipping_data[add_rates][{$destination_id}][rate_value][{$smarty.const.DISTANCE_RATE_INDEX}][0][amount]" data-v-min="0" data-v-max="999999" data-p-sign="s" data-a-sign=" {__("meters")}" size="5" value="" class="cm-numeric input-small input-hidden" /></td>
                    <td>
                        <input type="text" name="shipping_data[add_rates][{$destination_id}][rate_value][{$smarty.const.DISTANCE_RATE_INDEX}][0][value]" size="5" value="" class="input-small input-hidden" /></td>
                    <td>
                        <select class="input-medium" name="shipping_data[add_rates][{$destination_id}][rate_value][{$smarty.const.DISTANCE_RATE_INDEX}][0][type]">
                            <option value="F">{__("absolute")} ({$currencies.$primary_currency.symbol nofilter})</option>
                            <option value="P">{__("percent")} (%)</option>
                        </select></td>
                    <td>
                        {hook name="shippings:distance_dependences_new"}
                        {/hook}</td>
                    <td class="right"> <div class="hidden-tools">{include file="buttons/multiple_buttons.tpl" item_id="add_rate_celm_distance_`$destination_id`" tag_level=3}</div></td>
                </tr>
            {/if}

        </table>
    </div>

{/if}